﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18034
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SLSTOffCampusWeb.ABSAMP {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="urn:BeachBasics.wsdl", ConfigurationName="ABSAMP.BeachBasicsPortType")]
    public interface BeachBasicsPortType {
        
        // CODEGEN: Generating message contract since the wrapper namespace (https://www.surfguard.slsa.asn.au/SLSA_WebServices/modules/absamp/BeachBasics.php) of message getBeachListRequest does not match the default value (urn:BeachBasics.wsdl)
        [System.ServiceModel.OperationContractAttribute(Action="urn:getBeachList", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SLSTOffCampusWeb.ABSAMP.getBeachListResponse getBeachList(SLSTOffCampusWeb.ABSAMP.getBeachListRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:getBeachList", ReplyAction="*")]
        System.Threading.Tasks.Task<SLSTOffCampusWeb.ABSAMP.getBeachListResponse> getBeachListAsync(SLSTOffCampusWeb.ABSAMP.getBeachListRequest request);
        
        // CODEGEN: Generating message contract since the wrapper namespace (https://www.surfguard.slsa.asn.au/SLSA_WebServices/modules/absamp/BeachBasics.php) of message getBeachRequest does not match the default value (urn:BeachBasics.wsdl)
        [System.ServiceModel.OperationContractAttribute(Action="urn:getBeach", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        SLSTOffCampusWeb.ABSAMP.getBeachResponse getBeach(SLSTOffCampusWeb.ABSAMP.getBeachRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:getBeach", ReplyAction="*")]
        System.Threading.Tasks.Task<SLSTOffCampusWeb.ABSAMP.getBeachResponse> getBeachAsync(SLSTOffCampusWeb.ABSAMP.getBeachRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.18034")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:BeachBasics.wsdl")]
    public partial class ctAuthentication : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string usernameField;
        
        private string passwordField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="token", Order=0)]
        public string username {
            get {
                return this.usernameField;
            }
            set {
                this.usernameField = value;
                this.RaisePropertyChanged("username");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="token", Order=1)]
        public string password {
            get {
                return this.passwordField;
            }
            set {
                this.passwordField = value;
                this.RaisePropertyChanged("password");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getBeachList", WrapperNamespace="https://www.surfguard.slsa.asn.au/SLSA_WebServices/modules/absamp/BeachBasics.php" +
        "", IsWrapped=true)]
    public partial class getBeachListRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="urn:BeachBasics.wsdl")]
        public SLSTOffCampusWeb.ABSAMP.ctAuthentication ctAuthentication;
        
        public getBeachListRequest() {
        }
        
        public getBeachListRequest(SLSTOffCampusWeb.ABSAMP.ctAuthentication ctAuthentication) {
            this.ctAuthentication = ctAuthentication;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getBeachListResponse", WrapperNamespace="https://www.surfguard.slsa.asn.au/SLSA_WebServices/modules/absamp/BeachBasics.php" +
        "", IsWrapped=true)]
    public partial class getBeachListResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="", Order=0)]
        public string output;
        
        public getBeachListResponse() {
        }
        
        public getBeachListResponse(string output) {
            this.output = output;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getBeach", WrapperNamespace="https://www.surfguard.slsa.asn.au/SLSA_WebServices/modules/absamp/BeachBasics.php" +
        "", IsWrapped=true)]
    public partial class getBeachRequest {
        
        [System.ServiceModel.MessageHeaderAttribute(Namespace="urn:BeachBasics.wsdl")]
        public SLSTOffCampusWeb.ABSAMP.ctAuthentication ctAuthentication;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="", Order=0)]
        public string beachKey;
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="", Order=1)]
        public string beachName;
        
        public getBeachRequest() {
        }
        
        public getBeachRequest(SLSTOffCampusWeb.ABSAMP.ctAuthentication ctAuthentication, string beachKey, string beachName) {
            this.ctAuthentication = ctAuthentication;
            this.beachKey = beachKey;
            this.beachName = beachName;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(WrapperName="getBeachResponse", WrapperNamespace="https://www.surfguard.slsa.asn.au/SLSA_WebServices/modules/absamp/BeachBasics.php" +
        "", IsWrapped=true)]
    public partial class getBeachResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="", Order=0)]
        public string output;
        
        public getBeachResponse() {
        }
        
        public getBeachResponse(string output) {
            this.output = output;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface BeachBasicsPortTypeChannel : SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class BeachBasicsPortTypeClient : System.ServiceModel.ClientBase<SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType>, SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType {
        
        public BeachBasicsPortTypeClient() {
        }
        
        public BeachBasicsPortTypeClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public BeachBasicsPortTypeClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public BeachBasicsPortTypeClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public BeachBasicsPortTypeClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SLSTOffCampusWeb.ABSAMP.getBeachListResponse SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType.getBeachList(SLSTOffCampusWeb.ABSAMP.getBeachListRequest request) {
            return base.Channel.getBeachList(request);
        }
        
        public string getBeachList(SLSTOffCampusWeb.ABSAMP.ctAuthentication ctAuthentication) {
            SLSTOffCampusWeb.ABSAMP.getBeachListRequest inValue = new SLSTOffCampusWeb.ABSAMP.getBeachListRequest();
            inValue.ctAuthentication = ctAuthentication;
            SLSTOffCampusWeb.ABSAMP.getBeachListResponse retVal = ((SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType)(this)).getBeachList(inValue);
            return retVal.output;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<SLSTOffCampusWeb.ABSAMP.getBeachListResponse> SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType.getBeachListAsync(SLSTOffCampusWeb.ABSAMP.getBeachListRequest request) {
            return base.Channel.getBeachListAsync(request);
        }
        
        public System.Threading.Tasks.Task<SLSTOffCampusWeb.ABSAMP.getBeachListResponse> getBeachListAsync(SLSTOffCampusWeb.ABSAMP.ctAuthentication ctAuthentication) {
            SLSTOffCampusWeb.ABSAMP.getBeachListRequest inValue = new SLSTOffCampusWeb.ABSAMP.getBeachListRequest();
            inValue.ctAuthentication = ctAuthentication;
            return ((SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType)(this)).getBeachListAsync(inValue);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        SLSTOffCampusWeb.ABSAMP.getBeachResponse SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType.getBeach(SLSTOffCampusWeb.ABSAMP.getBeachRequest request) {
            return base.Channel.getBeach(request);
        }
        
        public string getBeach(SLSTOffCampusWeb.ABSAMP.ctAuthentication ctAuthentication, string beachKey, string beachName) {
            SLSTOffCampusWeb.ABSAMP.getBeachRequest inValue = new SLSTOffCampusWeb.ABSAMP.getBeachRequest();
            inValue.ctAuthentication = ctAuthentication;
            inValue.beachKey = beachKey;
            inValue.beachName = beachName;
            SLSTOffCampusWeb.ABSAMP.getBeachResponse retVal = ((SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType)(this)).getBeach(inValue);
            return retVal.output;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<SLSTOffCampusWeb.ABSAMP.getBeachResponse> SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType.getBeachAsync(SLSTOffCampusWeb.ABSAMP.getBeachRequest request) {
            return base.Channel.getBeachAsync(request);
        }
        
        public System.Threading.Tasks.Task<SLSTOffCampusWeb.ABSAMP.getBeachResponse> getBeachAsync(SLSTOffCampusWeb.ABSAMP.ctAuthentication ctAuthentication, string beachKey, string beachName) {
            SLSTOffCampusWeb.ABSAMP.getBeachRequest inValue = new SLSTOffCampusWeb.ABSAMP.getBeachRequest();
            inValue.ctAuthentication = ctAuthentication;
            inValue.beachKey = beachKey;
            inValue.beachName = beachName;
            return ((SLSTOffCampusWeb.ABSAMP.BeachBasicsPortType)(this)).getBeachAsync(inValue);
        }
    }
}
