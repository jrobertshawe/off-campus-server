﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using SLSTOffCampusWeb.Models;
using System.Xml;
using System.Xml.Linq;
using SLSTOffCampusWeb.ABSAMPDetails;
using System.Web.Script.Serialization;
using System.Text;
using System.Data.Linq;
namespace SLSTOffCampusWeb.Controllers
{
    public class HomeController : Controller
    {
        private Entities db = new Entities();
        public ActionResult Index()
        {
            
        
           return Json("success");
        }
        #region Image
        public ActionResult SaveImage(HttpPostedFileBase imageFile, string fileName)
        {
            if (imageFile != null)
            {
                Image image = new Image();

                var binary = new byte[imageFile.ContentLength];
                imageFile.InputStream.Read(binary, 0, imageFile.ContentLength);
                image.fileName = imageFile.FileName;
                image.imgData = binary;
                db.Images.Add(image);
                db.SaveChanges();
            }
            return Content("success");
        }

        public ActionResult GetImage(string filename)
        {
            // The Name of the Upload component is "files"
            Image image = db.Images.SingleOrDefault(i => i.fileName == filename);
            if (image != null)
            {

                if (image.imgData == null)
                    return HttpNotFound();
                byte[] imageData = image.imgData.ToArray();
                return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/png");
            }
            return HttpNotFound();

        }


        #endregion

        #region ABSAMP

        public ActionResult UpdateABSAMPLocation()
        {
            var locations = db.LocationTemplates;

            foreach (LocationTemplate location in locations)
            {
                try
                {
                    BeachDetailsPortTypeClient absamp = new BeachDetailsPortTypeClient();
                    ctAuthentication auth = new ctAuthentication();
                    auth.username = "alivemobile";
                    auth.password = "Al1v3M081le";
                    string beachString = absamp.getBeachDetails(auth, location.beachKey, location.name);
                    XDocument doc = XDocument.Parse(beachString);
                    IEnumerable<XElement> rootCategory = doc.Descendants("Beach");
                    string Latitude = rootCategory.Descendants("Latitude").FirstOrDefault().Value;
                    string Longitude = rootCategory.Descendants("Longitude").FirstOrDefault().Value;
                    string Description = rootCategory.Descendants("Description").FirstOrDefault().Value;
                    string HazardRating = rootCategory.Descendants("HazardRating").FirstOrDefault().Value;
                    location.notes = Description;
                    location.latitude = Latitude;
                    location.longitude = Longitude;
                    location.hazardRating = HazardRating;
                }
                catch
                {

                }


            }
            db.SaveChanges();
            return Json("success");
        }
        public ActionResult RebuildABSAMPList()
        {
            string key;
            string name;
            SLSTOffCampusWeb.ABSAMP.BeachBasicsPortTypeClient absamp = new SLSTOffCampusWeb.ABSAMP.BeachBasicsPortTypeClient();
            SLSTOffCampusWeb.ABSAMP.ctAuthentication auth = new SLSTOffCampusWeb.ABSAMP.ctAuthentication();
            auth.username = "alivemobile";
            auth.password = "Al1v3M081le";
            string beaches = absamp.getBeachList(auth);
            XmlReaderSettings set = new XmlReaderSettings();
            XDocument doc = XDocument.Parse(beaches);
            IEnumerable<XElement> rootCategory = doc.Descendants("Beach");
            foreach (XElement childBeach in rootCategory)
            {
                key = childBeach.Attribute("key").Value;
                name = childBeach.Attribute("name").Value;
                if (key.ToUpper().Contains("TAS") == true)
                {

                    if (db.LocationTemplates.SingleOrDefault(b => b.beachKey == key) == null)
                    {
                        LocationTemplate template = new LocationTemplate();

                        template.beachKey = key;
                        template.name = name;
                        db.LocationTemplates.Add(template);
                        db.SaveChanges();
                    }
                }

            }
            return Json("success");
        }
        #endregion

        #region Interface
        [HttpPost]
        public ActionResult StoreFeedback(FormCollection collection)
        {
            string json = collection["json"];
            if (json == null)
                return Json("failure", JsonRequestBehavior.AllowGet);
            DateTime latestTime = DateTime.MinValue;
            FeedBackRequest requestItem = JsonConvert.DeserializeObject<FeedBackRequest>(json);
            Feedback feedback = new Feedback();
            feedback.name = requestItem.name;
            feedback.email = requestItem.email;
            feedback.title = requestItem.title;
            feedback.description = requestItem.description;
            feedback.created = DateTime.Now;
            db.Feedbacks.Add(feedback);
            db.SaveChanges();


            return Json("success");
        }
        [HttpPost]
        public ActionResult registerUser(FormCollection collection)
        {
            string json = collection["json"];
            registerUser requestItem = JsonConvert.DeserializeObject<registerUser>(json);
            registerUserResponseModel responsejson = new registerUserResponseModel();
            Teacher userrecord = db.Teachers.SingleOrDefault(i => i.email == requestItem.email);
            if (userrecord != null)
            {
                responsejson.errorMsg = "Email already exists";
                responsejson.error = 1;
                return Json(responsejson);

            }
            userrecord = new Teacher();
            userrecord.email = requestItem.email;
            userrecord.password = requestItem.password;
            userrecord.firstname = requestItem.firstname;
            userrecord.lastname = requestItem.lastname;
            responsejson.error = 0;
            db.Teachers.Add(userrecord);
            db.SaveChanges();
            return Json(responsejson);
        }
        [HttpPost]
        public ActionResult loginUser(FormCollection collection)
        {
            string json = collection["json"];
            registerUser requestItem = JsonConvert.DeserializeObject<registerUser>(json);
            registerUserResponseModel responsejson = new registerUserResponseModel();
            Teacher userrecord = db.Teachers.SingleOrDefault(i => i.email.ToLower() == requestItem.email.ToLower() && i.password == requestItem.password);
            if (userrecord == null)
            {
                responsejson.errorMsg = "Email or password is not valid";
                responsejson.error = 1;
                return Json(responsejson);

            }
            responsejson.error = 0;
            responsejson.token = userrecord.id.ToString();

            return Json(responsejson);

        }
        [HttpPost]
        public ActionResult forgotPassword(FormCollection collection)
        {
            string json = collection["json"];
            registerUser requestItem = JsonConvert.DeserializeObject<registerUser>(json);
            registerUserResponseModel responsejson = new registerUserResponseModel();
            user userrecord = db.users.SingleOrDefault(i => i.email == requestItem.email);
            if (userrecord == null)
            {
                responsejson.errorMsg = "Email is not valid";
                responsejson.error = 1;
                return Json(responsejson);

            }
            responsejson.error = 0;
             return Json(responsejson);

        }
        [HttpPost]
        public ActionResult SyncRecordRequest(FormCollection collection)
        {
            string json = collection["json"];
            if (json == null)
                return Json("failure", JsonRequestBehavior.AllowGet);
            SyncObjectRequestModel requestItem = JsonConvert.DeserializeObject<SyncObjectRequestModel>(json);
            SyncRecordResponseModel response = new SyncRecordResponseModel();
            response.status.error = 1;
            response.status.errorMsg = "failed";
            if (requestItem.action == null)
            {
                response.status.error = 1;
                response.status.errorMsg = "no command set";
                return Json(response);
            }
            switch (requestItem.action.ToUpper())
            {
                case "ADD":
                    response = AddRecord(requestItem);
                    break;
                case "UPDATE":
                    response = UpdateRecord(requestItem);
                    break;
                case "DELETE":
                    response = DeleteRecord(requestItem);
                    break;
                default:
                    response = new SyncRecordResponseModel();
                    response.status.error = 1;
                    response.status.errorMsg = "Action is not valid";
                    break;

            }
            return Json(response);
        }
        [HttpPost]
        public ActionResult SyncChangesRequest(FormCollection collection)
        {
            string json = collection["json"];
            if (json == null)
                return Json("failure", JsonRequestBehavior.AllowGet);
            SyncChangesRequestModel requestItem = JsonConvert.DeserializeObject<SyncChangesRequestModel>(json);
            SyncChangesResponse response = new SyncChangesResponse();
            response.status.error = 1;
            response.status.errorMsg = "failed";
            DateTime latestTime = DateTime.MinValue;
            DateTime requestTime = DateTime.MinValue;
            DateTime tableTime = DateTime.MinValue;
            DateTime tempDate;
            try
            {
                requestTime = DateTime.Parse(requestItem.updateTime);
                latestTime = requestTime;
            }
            catch
            {

            }
            #region LOCATIONTEMPLATE
            var locationTemplateFound = db.LocationTemplates.Where(b => b.modified > requestTime && b.riskType != null);
            if (locationTemplateFound.Count() > 0)
            {
                foreach (LocationTemplate locationTemplate in locationTemplateFound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "LocationTemplate";
                    item.record.serverId = locationTemplate.id.ToString();
                    item.record.recordData.Add("address", locationTemplate.address);
                    item.record.recordData.Add("locationType", locationTemplate.locationType);
                    item.record.recordData.Add("beachKey", locationTemplate.beachKey);
                    item.record.recordData.Add("category", locationTemplate.category);
                    item.record.recordData.Add("facilities", locationTemplate.facilities);
                    item.record.recordData.Add("hazardRating", locationTemplate.hazardRating);
                    item.record.recordData.Add("latitude", locationTemplate.latitude);
                    item.record.recordData.Add("longitude", locationTemplate.longitude);
                    item.record.recordData.Add("name", locationTemplate.name);
                    item.record.recordData.Add("notes", locationTemplate.notes);
                    item.record.recordData.Add("riskType", locationTemplate.riskType);
                    item.record.recordData.Add("delete", locationTemplate.deleted.ToString());
                    response.response.record.Add(item);
                }
                tableTime = (DateTime)(from d in db.LocationTemplates select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region SCHOOLRESPONSE
            var schoolFound = db.Schools.Where(b => b.modified > requestTime);
            if (schoolFound.Count() > 0)
            {
                foreach (School school in schoolFound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "School";
                    item.record.serverId = school.id.ToString();
                    item.record.recordData.Add("fax", school.fax);
                    item.record.recordData.Add("name", school.name);
                    item.record.recordData.Add("phone", school.phone);
                    item.record.recordData.Add("postaladdress", school.postaladdress);
                    item.record.recordData.Add("streetaddress", school.streetaddress);
                    item.record.recordData.Add("category", school.category);
                    item.record.recordData.Add("delete", school.deleted.ToString());
                    item.record.recordData.Add("approverEmail", school.approverEmail);
                    item.record.recordData.Add("approverName", school.approverName);
                    response.response.record.Add(item);
                }
                tableTime = (DateTime)(from d in db.Schools select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region TEACHERRESPONSE
            var teacherFound = db.Teachers.Where(b => b.modified > requestTime);
            if (teacherFound.Count() > 0)
            {
                foreach (Teacher teacher in teacherFound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.parent.tableName = "School";
                    item.parent.serverId = teacher.schoolId.ToString();

                    item.record.tableName = "Teacher";
                    item.record.serverId = teacher.id.ToString();
                    item.record.recordData.Add("firstname", teacher.firstname);
                    item.record.recordData.Add("lastname", teacher.lastname);
                    item.record.recordData.Add("password", teacher.password);
                    item.record.recordData.Add("email", teacher.email);
                    item.record.recordData.Add("avatar", teacher.avatar);
                    item.record.recordData.Add("parentId", teacher.schoolId.ToString());
                    item.record.recordData.Add("delete", teacher.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.Teachers select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region ACTIVITY
        
            var activityFound = db.Activities.Where(b => b.modified > requestTime);
            if (activityFound.Count() > 0)
            {
                foreach (Activity activity in activityFound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "Activity";
                    item.parent.tableName = "Teacher";
                    item.parent.serverId = activity.Teacher.id.ToString();
                    item.record.serverId = activity.id.ToString();
                    item.record.recordData.Add("status", activity.status);
                    if (activity.dateEnd != null)
                    {
                        tempDate = (DateTime)activity.dateEnd;
                        item.record.recordData.Add("dateEnd", tempDate.ToString("s"));
                    }
                    if (activity.dateStart != null)
                    {
                        tempDate = (DateTime)activity.dateStart;
                        item.record.recordData.Add("dateStart", tempDate.ToString("s"));
                    }
                     item.record.recordData.Add("name", activity.name);
                     item.record.recordData.Add("type", activity.type);
                    item.record.recordData.Add("numberOfStudents", activity.numberOfStudents.ToString());
                    item.record.recordData.Add("schoolClasName", activity.schoolClasName);
                    item.record.recordData.Add("approverComment", activity.approverComment);
                    item.record.recordData.Add("parentId", activity.teacherId.ToString());
                    item.record.recordData.Add("delete", activity.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.Activities select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region LOCATION
            var locationfound = db.Locations.Where(b => b.modified > requestTime);
            if (locationfound.Count() > 0)
            {
                foreach (Location location in locationfound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "Location";
                    item.parent.tableName = "Activity";
                    item.parent.serverId = location.Activity.id.ToString();
               
                    item.record.serverId = location.id.ToString();
                    item.record.recordData.Add("address", location.address);
                    item.record.recordData.Add("facilities", location.facilities);
                    item.record.recordData.Add("latitude", location.latitude);
                    item.record.recordData.Add("longitude", location.longitude);
                    item.record.recordData.Add("name", location.name);
                    item.record.recordData.Add("notes", location.notes);
                    item.record.recordData.Add("riskType", location.riskType);
                    if (location.isPrimary != null)
                     item.record.recordData.Add("isPrimary", location.isPrimary.ToString());
                    if (location.hazardRating != null)
                     item.record.recordData.Add("hazardRating", location.hazardRating.ToString());
                    item.record.recordData.Add("parentId", location.activityId.ToString());
                    item.record.recordData.Add("delete", location.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.Teachers select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region ACCREDITATION
            var accreditationfound = db.Accreditations.Where(b => b.modified > requestTime);
            if (accreditationfound.Count() > 0)
            {
                foreach (Accreditation accreditation in accreditationfound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "Accreditation";
                    item.parent.tableName = "Teacher";
                    item.parent.serverId = accreditation.Teacher.id.ToString();
                    item.record.serverId = accreditation.id.ToString();
                    item.record.recordData.Add("name", accreditation.name);
                    item.record.recordData.Add("reminderEnabled", accreditation.reminderEnabled.ToString());
                    if (accreditation.renewalDate != null)
                    {
                        tempDate = (DateTime)accreditation.renewalDate;
                        item.record.recordData.Add("renewalDate", tempDate.Date.ToString("s"));
                    }
                    item.record.recordData.Add("parentId", accreditation.teacherId.ToString());
                    item.record.recordData.Add("delete", accreditation.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.Accreditations select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region TEACHERSUPPORT
            var teacherSupportfound = db.TeacherSupports.Where(b => b.modified > requestTime);
            if (teacherSupportfound.Count() > 0)
            {
                foreach (TeacherSupport teacherSupport in teacherSupportfound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "TeacherSupport";
                    item.parent.tableName = "Activity";
                    item.parent.serverId = teacherSupport.Activity.id.ToString();
                    item.record.serverId = teacherSupport.id.ToString();
                    item.record.recordData.Add("email", teacherSupport.email);
                    item.record.recordData.Add("parentId", teacherSupport.activityId.ToString());
                    item.record.recordData.Add("delete", teacherSupport.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.TeacherSupports select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region COMMENT
            var commentfound = db.Comments.Where(b => b.modified > requestTime);
            if (commentfound.Count() > 0)
            {
                foreach (Comment comment in commentfound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "Comment";
                    item.record.serverId = comment.id.ToString();
                    item.parent.tableName = "Activity";
                    item.parent.serverId = comment.Activity.id.ToString();
                
                    if (comment.date != null)
                    {
                        tempDate = (DateTime)comment.date;
                        item.record.recordData.Add("renewalDate", tempDate.Date.ToString("s"));
                    }
                    item.record.recordData.Add("comment", comment.comment1);
                    item.record.recordData.Add("parentId", comment.activityId.ToString());
                    item.record.recordData.Add("delete", comment.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.Comments select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region ActivityLogNote
            var activityLogNotesFond = db.ActivityLogNotes.Where(b => b.modified > requestTime);
            if (activityLogNotesFond.Count() > 0)
            {
                foreach (ActivityLogNote activityLogNote in activityLogNotesFond)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "ActivityLogNote";
                    item.record.serverId = activityLogNote.id.ToString();
                    item.parent.tableName = "Activity";
                    item.parent.serverId = activityLogNote.Activity.id.ToString();

                    if (activityLogNote.date != null)
                    {
                        tempDate = (DateTime)activityLogNote.date;
                        item.record.recordData.Add("date", tempDate.Date.ToString("s"));
                    }
                    item.record.recordData.Add("avatar", activityLogNote.avatar);
                    item.record.recordData.Add("logNote", activityLogNote.logNote);
                    item.record.recordData.Add("name", activityLogNote.name);
                    item.record.recordData.Add("parentId", activityLogNote.activityId.ToString());
                    item.record.recordData.Add("delete", activityLogNote.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.ActivityLogNotes select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region NOTE
            var notefound = db.Notes.Where(b => b.modified > requestTime);
            if (notefound.Count() > 0)
            {
                foreach (Note note in notefound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "Note";
                    item.record.serverId = note.id.ToString();
                    item.parent.tableName = "Activity";
                    item.parent.serverId = note.Activity.id.ToString();

                    if (note.date != null)
                    {
                        tempDate = (DateTime)note.date;
                        item.record.recordData.Add("date", tempDate.Date.ToString("s"));
                    }
                    if (note.note1 != null)
                    {
                        item.record.recordData.Add("note", note.note1);
                    }
                    item.record.recordData.Add("photo", note.photo);
                    item.record.recordData.Add("teacherEmailID", note.teacherEmailID);
                    item.record.recordData.Add("parentId", note.activityId.ToString());
                    item.record.recordData.Add("delete", note.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.Notes select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region CHECKLIST
            var checklistfound = db.Checklists.Where(b => b.modified > requestTime);
            if (checklistfound.Count() > 0)
            {
                foreach (Checklist checklist in checklistfound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "CheckList";
                    item.parent.tableName = "Activity";
                    item.parent.serverId = checklist.Activity.id.ToString();
                
                    item.record.serverId = checklist.id.ToString();
                    item.record.recordData.Add("name", checklist.name);
                    item.record.recordData.Add("checklistType", checklist.checklistType);
                    item.record.recordData.Add("draftActivityURI", checklist.draftActivityURI);
                    item.record.recordData.Add("checklistValues", checklist.checklistValues);
                    item.record.recordData.Add("parentId", checklist.activityId.ToString());
                    item.record.recordData.Add("delete", checklist.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.Checklists select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region TRANSPORT
            var transportfound = db.Transports.Where(b => b.modified > requestTime);
            if (transportfound.Count() > 0)
            {
                foreach (Transport transport in transportfound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "Transport";
                    item.parent.tableName = "Activity";
                    item.parent.serverId = transport.Activity.id.ToString();

                    item.record.serverId = transport.id.ToString();
                    item.record.recordData.Add("type", transport.type);
                    item.record.recordData.Add("pickLocation", transport.pickLocation);
                    item.record.recordData.Add("dropOffLocation", transport.dropOffLocation);
                    item.record.recordData.Add("parentId", transport.activityId.ToString());
                    if (transport.dropOffDate != null)
                    {
                        tempDate = (DateTime)transport.dropOffDate;
                        item.record.recordData.Add("dropOffDate", tempDate.ToString("s"));
                    }
                    item.record.recordData.Add("delete", transport.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.Transports select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            #region EmergencyContact
            var EmergencyContactFound = db.EmergencyContacts.Where(b => b.modified > requestTime);
            if (EmergencyContactFound.Count() > 0)
            {
                foreach (EmergencyContact emergencyContacts in EmergencyContactFound)
                {
                    ResponseRecordModel item = new ResponseRecordModel();
                    item.record.tableName = "EmergencyContact";
                    item.parent.tableName = "Activity";
                    item.parent.serverId = emergencyContacts.Activity.id.ToString();

                    item.record.serverId = emergencyContacts.id.ToString();
                    item.record.recordData.Add("name", emergencyContacts.name);
                    item.record.recordData.Add("phone", emergencyContacts.phone);
                    item.record.recordData.Add("parentId", emergencyContacts.activityId.ToString());
                    item.record.recordData.Add("delete", emergencyContacts.deleted.ToString());
                    response.response.record.Add(item);
                }
                latestTime = (DateTime)(from d in db.EmergencyContacts select d.modified).Max();
                int i = DateTime.Compare(latestTime, tableTime);
                if (i < 0)  //latesttime is less than tabletime
                    latestTime = tableTime;
            }
            #endregion
            response.response.updateTime = latestTime.ToString("s");
            response.status.error = 0;
            response.status.errorMsg = "";
            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;

            var result = new ContentResult
            {
                Content = serializer.Serialize(response),
                ContentType = "application/json"
            };
            return result;
        }
        private SyncRecordResponseModel UpdateRecord(SyncObjectRequestModel requestItem)
        {
            SyncRecordResponseModel response = new SyncRecordResponseModel();



            switch (requestItem.record.tableName.ToUpper())
            {
                case "SCHOOL":
                    response.response.record.serverId = UpdateSchool(requestItem);
                    break;
                case "TEACHER":
                    response.response.record.serverId = UpdateTeacher(requestItem);
                    break;
                case "LOCATION":
                    response.response.record.serverId = UpdateLocation(requestItem);
                    break;
                case "LOCATIONTEMPLATE":
                    response.response.record.serverId = UpdateLocationTemplate(requestItem);
                    break;
                case "ACCREDITATION":
                    response.response.record.serverId = UpdateAccreditation(requestItem);
                    break;
                case "ACTIVITY":
                    response.response.record.serverId = UpdateActivity(requestItem);
                    break;
                case "TEACHERSUPPORT":
                    response.response.record.serverId = UpdateTeacherSupport(requestItem);
                    break;
                case "COMMENT":
                    response.response.record.serverId = UpdateComment(requestItem);
                    break;
                case "ALERT":
                    response.response.record.serverId = UpdateAlert(requestItem);
                    break;
                case "GENERALWEATHER":
                    response.response.record.serverId = UpdateGeneralWeather(requestItem);
                    break;
                case "FORECAST":
                    response.response.record.serverId = UpdateForecast(requestItem);
                    break;
                case "CHECKLIST":
                    response.response.record.serverId = UpdateCheckList(requestItem);
                    break;
                case "PHOTO":
                    response.response.record.serverId = UpdatePhoto(requestItem);
                    break;
                case "TRANSPORT":
                    response.response.record.serverId = UpdateTransport(requestItem);
                    break;
                case "EMERGENCYCONTACT":
                    response.response.record.serverId = UpdateEmergencyContact(requestItem);
                    break;
                case "NOTE":
                    response.response.record.serverId = UpdateNote(requestItem);
                    break;
                default:
                    response.status.error = 1;
                    response.status.errorMsg = "Table is not valid";
                    break;

            }
            return response;
        }
        private SyncRecordResponseModel DeleteRecord(SyncObjectRequestModel requestItem)
        {
            SyncRecordResponseModel response = new SyncRecordResponseModel();

            switch (requestItem.record.tableName.ToUpper())
            {
                case "SCHOOL":
                    response.response.record.serverId = DeleteSchool(requestItem);
                    break;
                case "TEACHER":
                    response.response.record.serverId = DeleteTeacher(requestItem);
                    break;
                case "LOCATION":
                    response.response.record.serverId = DeleteLocation(requestItem);
                    break;
                case "LOCATIONTEMPLATE":
                    response.response.record.serverId = DeleteLocationTemplate(requestItem);
                    break;
                case "ACCREDITATION":
                    response.response.record.serverId = DeleteAccreditation(requestItem);
                    break;
                case "ACTIVITY":
                    response.response.record.serverId = DeleteActivity(requestItem);
                    break;
                case "TEACHERSUPPORT":
                    response.response.record.serverId = DeleteTeacherSupport(requestItem);
                    break;
                case "COMMENT":
                    response.response.record.serverId = DeleteComment(requestItem);
                    break;
                case "ALERT":
                    response.response.record.serverId = DeleteAlert(requestItem);
                    break;
                case "GENERALWEATHER":
                    response.response.record.serverId = DeleteGeneralWeather(requestItem);
                    break;
                case "FORECAST":
                    response.response.record.serverId = DeleteForecast(requestItem);
                    break;
                case "CHECKLIST":
                    response.response.record.serverId = DeleteCheckList(requestItem);
                    break;
                case "PHOTO":
                    response.response.record.serverId = DeletePhoto(requestItem);
                    break;
                case "TRANSPORT":
                    response.response.record.serverId = DeleteTransport(requestItem);
                    break;
                case "EMERGENCYCONTACT":
                    response.response.record.serverId = DeleteEmergencyContact(requestItem);
                    break;
                case "NOTE":
                    response.response.record.serverId = DeleteNote(requestItem);
                    break;
                default:
                    response.status.error = 1;
                    response.status.errorMsg = "Table is not valid";
                    break;

            }
            return response;
        }
        private SyncRecordResponseModel AddRecord(SyncObjectRequestModel requestItem)
        {
            SyncRecordResponseModel response = new SyncRecordResponseModel();

            if (requestItem.record.coreDataId.Length == 0)
            {
                response.status.error = 1;
                response.status.errorMsg = "coreDataId value is not set";
                return response;
            }

            response.response.record.tableName = requestItem.record.tableName;
            response.response.record.coreDataId = requestItem.record.coreDataId;
            switch (requestItem.record.tableName.ToUpper())
            {
                case "SCHOOL":
                    response.response.record.serverId = AddSchool(requestItem);
                    break;
                case "TEACHER":
                    response.response.record.serverId = AddTeacher(requestItem);
                    break;
                case "LOCATION":
                    response.response.record.serverId = AddLocation(requestItem);
                    break;
                case "LOCATIONTEMPLATE":
                    response.response.record.serverId = AddLocationTemplate(requestItem);
                    break;
                case "ACCREDITATION":
                    response.response.record.serverId = AddAccreditation(requestItem);
                    break;
                case "ACTIVITY":
                    response.response.record.serverId = AddActivity(requestItem);
                    break;
                case "TEACHERSUPPORT":
                    response.response.record.serverId = AddTeacherSupport(requestItem);
                    break;
                case "COMMENT":
                    response.response.record.serverId = AddComment(requestItem);
                    break;
                case "ALERT":
                    response.response.record.serverId = AddAlert(requestItem);
                    break;
                case "GENERALWEATHER":
                    response.response.record.serverId = AddGeneralWeather(requestItem);
                    break;
                case "FORECAST":
                    response.response.record.serverId = AddForecast(requestItem);
                    break;
                case "CHECKLIST":
                    response.response.record.serverId = AddCheckList(requestItem);
                    break;
                case "PHOTO":
                    response.response.record.serverId = AddPhoto(requestItem);
                    break;
                case "TRANSPORT":
                    response.response.record.serverId = AddTransport(requestItem);
                    break;
                case "EMERGENCYCONTACT":
                    response.response.record.serverId = AddEmergencyContact(requestItem);
                    break;
                case "NOTE":
                    response.response.record.serverId = AddNote(requestItem);
                    break;
                default:
                    response.status.error = 1;
                    response.status.errorMsg = "Table is not valid";
                    break;

            }
            return response;
        }
        #endregion

        #region LocationTemplate
        //****************LocationTemplate ADD UPDATE and DELETE
        private string AddLocationTemplate(SyncObjectRequestModel requestItem)
        {
            LocationTemplate locationTemplate = db.LocationTemplates.SingleOrDefault(i => i.coreDataId == requestItem.record.coreDataId);
            if (locationTemplate == null)
            {
                locationTemplate = new LocationTemplate();
                db.LocationTemplates.Add(locationTemplate);
                locationTemplate.coreDataId = requestItem.record.coreDataId;

            }
            if (requestItem.record.recordData.ContainsKey("address"))
            {
                string value = requestItem.record.recordData["address"];
                locationTemplate.address = value;
            }
            if (requestItem.record.recordData.ContainsKey("altitude"))
            {
                string value = requestItem.record.recordData["altitude"];
                locationTemplate.altitude = value;
            }
            if (requestItem.record.recordData.ContainsKey("facilities"))
            {
                string value = requestItem.record.recordData["facilities"];
                locationTemplate.facilities = value;
            }
            if (requestItem.record.recordData.ContainsKey("latitude"))
            {
                string value = requestItem.record.recordData["latitude"];
                locationTemplate.latitude = value;
            }
            if (requestItem.record.recordData.ContainsKey("locationType"))
            {
                string value = requestItem.record.recordData["locationType"];
                locationTemplate.locationType = value;
            }
            if (requestItem.record.recordData.ContainsKey("longitude"))
            {
                string value = requestItem.record.recordData["longitude"];
                locationTemplate.longitude = value;
            }
            if (requestItem.record.recordData.ContainsKey("name"))
            {
                string value = requestItem.record.recordData["name"];
                locationTemplate.name = value;
            }
            if (requestItem.record.recordData.ContainsKey("notes"))
            {
                string value = requestItem.record.recordData["notes"];
                locationTemplate.notes = value;
            }
            if (requestItem.record.recordData.ContainsKey("riskType"))
            {
                string value = requestItem.record.recordData["riskType"];
                locationTemplate.riskType = value;
            }
            if (requestItem.record.recordData.ContainsKey("hazardRating"))
            {
                string value = requestItem.record.recordData["hazardRating"];
                locationTemplate.riskType = value;



            }

            locationTemplate.deleted = false;
            locationTemplate.modified = DateTime.Now;
            db.SaveChanges();
            return locationTemplate.id.ToString(); ;
        }
        private string UpdateLocationTemplate(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            LocationTemplate locationTemplate = db.LocationTemplates.SingleOrDefault(i => i.id == serverId);
            if (locationTemplate != null)
            {
                // See whether Dictionary contains this string.
                if (requestItem.record.recordData.ContainsKey("address"))
                {
                    string value = requestItem.record.recordData["address"];
                    locationTemplate.address = value;
                }
                if (requestItem.record.recordData.ContainsKey("altitude"))
                {
                    string value = requestItem.record.recordData["altitude"];
                    locationTemplate.altitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("facilities"))
                {
                    string value = requestItem.record.recordData["facilities"];
                    locationTemplate.facilities = value;
                }
                if (requestItem.record.recordData.ContainsKey("latitude"))
                {
                    string value = requestItem.record.recordData["latitude"];
                    locationTemplate.latitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("locationType"))
                {
                    string value = requestItem.record.recordData["locationType"];
                    locationTemplate.locationType = value;
                }
                if (requestItem.record.recordData.ContainsKey("longitude"))
                {
                    string value = requestItem.record.recordData["longitude"];
                    locationTemplate.longitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    locationTemplate.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("notes"))
                {
                    string value = requestItem.record.recordData["notes"];
                    locationTemplate.notes = value;
                }
                if (requestItem.record.recordData.ContainsKey("riskType"))
                {
                    string value = requestItem.record.recordData["riskType"];
                    locationTemplate.riskType = value;
                }
                if (requestItem.record.recordData.ContainsKey("hazardRating"))
                {
                    string value = requestItem.record.recordData["hazardRating"];
                    locationTemplate.riskType = value;



                }
                locationTemplate.modified = DateTime.Now;

                db.SaveChanges();
                return locationTemplate.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteLocationTemplate(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            LocationTemplate locationTemplate = db.LocationTemplates.SingleOrDefault(i => i.id == serverId);
            if (locationTemplate != null)
            {
                locationTemplate.deleted = true;
                locationTemplate.modified = DateTime.Now;
                db.SaveChanges();
                return locationTemplate.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        //*****************END Location ADD UPDATE and DELETE
        #endregion


        #region School
        //****************School ADD UPDATE and DELETE
        private string AddSchool(SyncObjectRequestModel requestItem)
        {
            School school = db.Schools.SingleOrDefault(i => i.coreDataId == requestItem.record.coreDataId);
            if (school == null)
            {
                school = new School();
                db.Schools.Add(school);
                school.coreDataId = requestItem.record.coreDataId;

            }
            // See whether Dictionary contains this string.
            if (requestItem.record.recordData.ContainsKey("name"))
            {
                string value = requestItem.record.recordData["name"];
                school.name = value;
            }
            if (requestItem.record.recordData.ContainsKey("postaladdress"))
            {
                string value = requestItem.record.recordData["postaladdress"];
                school.postaladdress = value;
            }
            if (requestItem.record.recordData.ContainsKey("streetaddress"))
            {
                string value = requestItem.record.recordData["streetaddress"];
                school.streetaddress = value;
            }
            if (requestItem.record.recordData.ContainsKey("phone"))
            {
                string value = requestItem.record.recordData["phone"];
                school.phone = value;
            }
            if (requestItem.record.recordData.ContainsKey("category"))
            {
                string value = requestItem.record.recordData["category"];
                school.category = value;
            }
            if (requestItem.record.recordData.ContainsKey("approverName"))
            {
                string value = requestItem.record.recordData["approverName"];
                school.approverName = value;
            
            }
            if (requestItem.record.recordData.ContainsKey("approverEmail"))
            {
                string value = requestItem.record.recordData["approverEmail"];
                school.approverEmail = value;
            } 
            school.deleted = false;
            school.modified = DateTime.Now;
            db.SaveChanges();
            return school.id.ToString(); ;
        }
        private string UpdateSchool(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            School school = db.Schools.SingleOrDefault(i => i.id == serverId);
            if (school != null)
            {
                // See whether Dictionary contains this string.
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    school.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("postaladdress"))
                {
                    string value = requestItem.record.recordData["postaladdress"];
                    school.postaladdress = value;
                }
                if (requestItem.record.recordData.ContainsKey("streetaddress"))
                {
                    string value = requestItem.record.recordData["streetaddress"];
                    school.streetaddress = value;
                }
                if (requestItem.record.recordData.ContainsKey("phone"))
                {
                    string value = requestItem.record.recordData["phone"];
                    school.phone = value;
                }
                if (requestItem.record.recordData.ContainsKey("category"))
                {
                    string value = requestItem.record.recordData["category"];
                    school.category = value;
                }
                if (requestItem.record.recordData.ContainsKey("approverName"))
                {
                    string value = requestItem.record.recordData["approverName"];
                    school.approverName = value;

                }
                if (requestItem.record.recordData.ContainsKey("approverEmail"))
                {
                    string value = requestItem.record.recordData["approverEmail"];
                    school.approverEmail = value;
                } 
                school.modified = DateTime.Now;
     
                db.SaveChanges();
                return school.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteSchool(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            School beach = db.Schools.SingleOrDefault(i => i.id == serverId);
            if (beach != null)
            {
                beach.deleted = true;
                beach.modified = DateTime.Now;
                db.SaveChanges();
                return beach.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        //*****************END School ADD UPDATE and DELETE
        #endregion

        #region Teacher
        //****************Teacher ADD UPDATE and DELETE
        private string AddTeacher(SyncObjectRequestModel requestItem)
        {
             Teacher teacher = new Teacher();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            School school = db.Schools.SingleOrDefault(i => i.id == parentId);
            if (school != null)
            {
                if (requestItem.record.recordData.ContainsKey("firstname"))
                {
                    string value = requestItem.record.recordData["firstname"];
                    teacher.firstname = value;
                }
                if (requestItem.record.recordData.ContainsKey("lastname"))
                {
                    string value = requestItem.record.recordData["lastname"];
                    teacher.lastname = value;
                }
                if (requestItem.record.recordData.ContainsKey("email"))
                {
                    string value = requestItem.record.recordData["email"];
                    teacher.email = value;
                }
                if (requestItem.record.recordData.ContainsKey("password"))
                {
                    string value = requestItem.record.recordData["password"];
                    teacher.password = value;
                }
                if (requestItem.record.recordData.ContainsKey("avatar"))
                {
                    string value = requestItem.record.recordData["avatar"];
                    teacher.avatar = value;
                }
                teacher.coreDataId = requestItem.record.coreDataId;
                teacher.deleted = false;
                school.Teachers.Add(teacher);
                teacher.modified = DateTime.Now;
                db.SaveChanges();
                return teacher.id.ToString(); ;
            }
            return "-1";
        }
           
        private string UpdateTeacher(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Teacher teacher = db.Teachers.SingleOrDefault(i => i.id == serverId);
            if (teacher != null)
            {
                if (requestItem.record.recordData.ContainsKey("firstname"))
                {
                    string value = requestItem.record.recordData["firstname"];
                    teacher.firstname = value;
                }
                if (requestItem.record.recordData.ContainsKey("lastname"))
                {
                    string value = requestItem.record.recordData["lastname"];
                    teacher.lastname = value;
                }
                if (requestItem.record.recordData.ContainsKey("email"))
                {
                    string value = requestItem.record.recordData["email"];
                    teacher.email = value;
                }
                if (requestItem.record.recordData.ContainsKey("password"))
                {
                    string value = requestItem.record.recordData["password"];
                    teacher.password = value;
                }
                if (requestItem.record.recordData.ContainsKey("avatar"))
                {
                    string value = requestItem.record.recordData["avatar"];
                    teacher.avatar = value;
                }
                teacher.coreDataId = requestItem.record.coreDataId;
                teacher.modified = DateTime.Now;
                db.SaveChanges();
                return teacher.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteTeacher(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Teacher teacher = db.Teachers.SingleOrDefault(i => i.id == serverId);
            if (teacher != null)
            {
                teacher.deleted = true;
                teacher.modified = DateTime.Now;
                db.SaveChanges();
                return teacher.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        //*****************END Teacher ADD UPDATE and DELETE
        #endregion
        #region  Accreditation
        private string AddAccreditation(SyncObjectRequestModel requestItem)
        {
            Accreditation accreditation = new Accreditation();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Teacher teacher = db.Teachers.SingleOrDefault(i => i.id == parentId);
            if (teacher != null)
            {
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    accreditation.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("reminderEnabled"))
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["reminderEnabled"], out resultint))
                    {
                        accreditation.reminderEnabled = resultint;
                    }

                
                    
                }
                if (requestItem.record.recordData.ContainsKey("renewalDate"))
                {
                    string value = requestItem.record.recordData["renewalDate"];
                    DateTime dateTime;
	                if (DateTime.TryParse(value, out dateTime))
	                {
	                        accreditation.renewalDate = dateTime;
	                }

                }
              
                accreditation.coreDataId = requestItem.record.coreDataId;
                accreditation.deleted = false;
                teacher.Accreditations.Add(accreditation);
                accreditation.modified = DateTime.Now;
                db.SaveChanges();
                return accreditation.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateAccreditation(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Accreditation accreditation = db.Accreditations.SingleOrDefault(i => i.id == serverId);
            if (accreditation != null)
            {
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    accreditation.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("reminderEnabled"))
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["reminderEnabled"], out resultint))
                    {
                        accreditation.reminderEnabled = resultint;
                    }



                }
                if (requestItem.record.recordData.ContainsKey("renewalDate"))
                {
                    string value = requestItem.record.recordData["renewalDate"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        accreditation.renewalDate = dateTime;
                    }

                }
                accreditation.modified = DateTime.Now;
                db.SaveChanges();
                return accreditation.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteAccreditation(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Accreditation accreditation = db.Accreditations.SingleOrDefault(i => i.id == serverId);
            if (accreditation != null)
            {
                accreditation.deleted = true;
                accreditation.modified = DateTime.Now;
                db.SaveChanges();
                return accreditation.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion

        #region Activity
        private string AddActivity(SyncObjectRequestModel requestItem)
        {
            Activity activity = new Activity();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Teacher teacher = db.Teachers.SingleOrDefault(i => i.id == parentId);
            if (teacher != null)
            {
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    activity.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("schoolClasName"))
                {
                    string value = requestItem.record.recordData["schoolClasName"];
                    activity.schoolClasName = value;
                }
                if (requestItem.record.recordData.ContainsKey("approverComment"))
                {
                    string value = requestItem.record.recordData["approverComment"];
                    activity.approverComment = value;
                }
                if (requestItem.record.recordData.ContainsKey("status"))
                {
                    string value = requestItem.record.recordData["status"];
                    activity.status = value;
                }
                if (requestItem.record.recordData.ContainsKey("status"))
                {
                    string value = requestItem.record.recordData["status"];
                    activity.status = value;
                }
                if (requestItem.record.recordData.ContainsKey("isFinishedSetup"))
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["isFinishedSetup"], out resultint))
                    {
                        activity.isFinishedSetup = resultint;
                    }



                }
                if (requestItem.record.recordData.ContainsKey("type"))
                {
                    int resultint;

                    string value = requestItem.record.recordData["type"];
                    activity.type = value;
                }
                
                if (requestItem.record.recordData.ContainsKey("uid"))
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["uid"], out resultint))
                    {
                        activity.uid = resultint;
                    }
                }
                

                if (requestItem.record.recordData.ContainsKey("lastSelectedIdx"))
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["lastSelectedIdx"], out resultint))
                    {
                        activity.lastSelectedIdx = resultint;
                    }



                }
                if (requestItem.record.recordData.ContainsKey("numberOfStudents"))
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["numberOfStudents"], out resultint))
                    {
                        activity.numberOfStudents = resultint;
                    }



                }
                if (requestItem.record.recordData.ContainsKey("dateCreated"))
                {
                    string value = requestItem.record.recordData["dateCreated"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        activity.dateCreated = dateTime;
                    }

                }
                if (requestItem.record.recordData.ContainsKey("dateEnd"))
                {
                    string value = requestItem.record.recordData["dateEnd"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        activity.dateEnd = dateTime;
                    }

                }
                if (requestItem.record.recordData.ContainsKey("dateStart"))
                {
                    string value = requestItem.record.recordData["dateStart"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        activity.dateStart = dateTime;
                    }

                }
                activity.coreDataId = requestItem.record.coreDataId;
                activity.deleted = false;
                teacher.Activities.Add(activity);
                activity.modified = DateTime.Now;
                db.SaveChanges();
                return activity.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateActivity(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == serverId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    activity.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("approverComment"))
                {
                    string value = requestItem.record.recordData["approverComment"];
                    activity.approverComment = value;
                }
                if (requestItem.record.recordData.ContainsKey("schoolClasName"))
                {
                    string value = requestItem.record.recordData["schoolClasName"];
                    activity.schoolClasName = value;
                }
                if (requestItem.record.recordData.ContainsKey("status"))
                {
                    string value = requestItem.record.recordData["status"];
                    activity.status = value;
                }
                if (requestItem.record.recordData.ContainsKey("status"))
                {
                    string value = requestItem.record.recordData["status"];
                    activity.status = value;
                }
                if (requestItem.record.recordData.ContainsKey("isFinishedSetup"))
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["isFinishedSetup"], out resultint))
                    {
                        activity.isFinishedSetup = resultint;
                    }



                }
                if (requestItem.record.recordData.ContainsKey("type"))
                {
                    string value = requestItem.record.recordData["type"];
                    activity.type = value;
           
                }

                if (requestItem.record.recordData.ContainsKey("uid"))
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["uid"], out resultint))
                    {
                        activity.uid = resultint;
                    }
                }


                if (requestItem.record.recordData.ContainsKey("lastSelectedIdx"))
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["lastSelectedIdx"], out resultint))
                    {
                        activity.lastSelectedIdx = resultint;
                    }



                }
                if (requestItem.record.recordData.ContainsKey("numberOfStudents"))
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["numberOfStudents"], out resultint))
                    {
                        activity.numberOfStudents = resultint;
                    }



                }
                if (requestItem.record.recordData.ContainsKey("dateCreated"))
                {
                    string value = requestItem.record.recordData["dateCreated"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        activity.dateCreated = dateTime;
                    }

                }
                if (requestItem.record.recordData.ContainsKey("dateEnd"))
                {
                    string value = requestItem.record.recordData["dateEnd"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        activity.dateEnd = dateTime;
                    }

                }
                if (requestItem.record.recordData.ContainsKey("dateStart"))
                {
                    string value = requestItem.record.recordData["dateStart"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        activity.dateStart = dateTime;
                    }

                }
                activity.modified = DateTime.Now;
                db.SaveChanges();
                return activity.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteActivity(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == serverId);
            if (activity != null)
            {
                activity.deleted = true;
                activity.modified = DateTime.Now;
                db.SaveChanges();
                return activity.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion

        #region TeacherSupport
        private string AddTeacherSupport(SyncObjectRequestModel requestItem)
        {
            TeacherSupport teachersupport = new TeacherSupport();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == parentId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("email"))
                {
                    string value = requestItem.record.recordData["email"];
                    teachersupport.email = value;
                }

                teachersupport.coreDataId = requestItem.record.coreDataId;
                teachersupport.deleted = false;
                activity.TeacherSupports.Add(teachersupport);
                teachersupport.modified = DateTime.Now;
                db.SaveChanges();
                return teachersupport.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateTeacherSupport(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            TeacherSupport teachersupport = db.TeacherSupports.SingleOrDefault(i => i.id == serverId);
            if (teachersupport != null)
            {
                if (requestItem.record.recordData.ContainsKey("email"))
                {
                    string value = requestItem.record.recordData["email"];
                    teachersupport.email = value;
                }
                teachersupport.modified = DateTime.Now;
                db.SaveChanges();
                return teachersupport.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteTeacherSupport(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            TeacherSupport teachersupport = db.TeacherSupports.SingleOrDefault(i => i.id == serverId);
            if (teachersupport != null)
            {
                teachersupport.deleted = true;
                teachersupport.modified = DateTime.Now;
                db.SaveChanges();
                return teachersupport.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region Comment
        private string AddComment(SyncObjectRequestModel requestItem)
        {
            Comment comment = new Comment();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == parentId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("comment"))
                {
                    string value = requestItem.record.recordData["comment"];
                    comment.comment1 = value;
                }
                if (requestItem.record.recordData.ContainsKey("uid"))
                {
                    string value = requestItem.record.recordData["uid"];
                    comment.uid = value;
                }
                if (requestItem.record.recordData.ContainsKey("date"))
                {
                    string value = requestItem.record.recordData["date"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        comment.date = dateTime;
                    }

                }
                comment.coreDataId = requestItem.record.coreDataId;
                comment.deleted = false;
                activity.Comments.Add(comment);
                comment.modified = DateTime.Now;
                db.SaveChanges();
                return comment.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateComment(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Comment comment = db.Comments.SingleOrDefault(i => i.id == serverId);
            if (comment != null)
            {
                    if (requestItem.record.recordData.ContainsKey("comment"))
                    {
                        string value = requestItem.record.recordData["comment"];
                        comment.comment1 = value;
                    }
                if (requestItem.record.recordData.ContainsKey("uid"))
                {
                    string value = requestItem.record.recordData["uid"];
                    comment.uid = value;
                }
                if (requestItem.record.recordData.ContainsKey("date"))
                {
                    string value = requestItem.record.recordData["date"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        comment.date = dateTime;
                    }

                }
                comment.modified = DateTime.Now;
                db.SaveChanges();
                return comment.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteComment(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Comment comment = db.Comments.SingleOrDefault(i => i.id == serverId);
            if (comment != null)
            {
                comment.deleted = true;
                comment.modified = DateTime.Now;
                db.SaveChanges();
                return comment.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region ActivityLogNote
        private string AddActivityLogNote(SyncObjectRequestModel requestItem)
        {
            ActivityLogNote activityLogNote = new ActivityLogNote();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == parentId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("logNote"))
                {
                    string value = requestItem.record.recordData["logNote"];
                    activityLogNote.logNote = value;
                }
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    activityLogNote.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("avatar"))
                {
                    string value = requestItem.record.recordData["avatar"];
                    activityLogNote.avatar = value;
                }
                if (requestItem.record.recordData.ContainsKey("date"))
                {
                    string value = requestItem.record.recordData["date"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        activityLogNote.date = dateTime;
                    }

                }
                activityLogNote.coreDataId = requestItem.record.coreDataId;
                activityLogNote.deleted = false;
                activity.ActivityLogNotes.Add(activityLogNote);
                activityLogNote.modified = DateTime.Now;
                db.SaveChanges();
                return activityLogNote.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateActivityLogNote(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            ActivityLogNote activityLogNote = db.ActivityLogNotes.SingleOrDefault(i => i.id == serverId);
            if (activityLogNote != null)
            {
                if (requestItem.record.recordData.ContainsKey("logNote"))
                {
                    string value = requestItem.record.recordData["logNote"];
                    activityLogNote.logNote = value;
                }
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    activityLogNote.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("avatar"))
                {
                    string value = requestItem.record.recordData["avatar"];
                    activityLogNote.avatar = value;
                }
                if (requestItem.record.recordData.ContainsKey("date"))
                {
                    string value = requestItem.record.recordData["date"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        activityLogNote.date = dateTime;
                    }

                }
                activityLogNote.modified = DateTime.Now;
                db.SaveChanges();
                return activityLogNote.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteActivityLogNote(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            ActivityLogNote activityLogNote = db.ActivityLogNotes.SingleOrDefault(i => i.id == serverId);
            if (activityLogNote != null)
         
            {
                activityLogNote.deleted = true;
                activityLogNote.modified = DateTime.Now;
                db.SaveChanges();
                return activityLogNote.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region Note
        private string AddNote(SyncObjectRequestModel requestItem)
        {
            Note note = new Note();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == parentId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("note"))
                {
                    string value = requestItem.record.recordData["note"];
                    note.note1 = value;
                }
                if (requestItem.record.recordData.ContainsKey("teacherEmailID"))
                {
                    string value = requestItem.record.recordData["teacherEmailID"];
                    note.teacherEmailID = value;
                }
                if (requestItem.record.recordData.ContainsKey("photo"))
                {
                    string value = requestItem.record.recordData["photo"];
                    note.photo = value;
                }
                if (requestItem.record.recordData.ContainsKey("date"))
                {
                    string value = requestItem.record.recordData["date"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        note.date = dateTime;
                    }

                }
                note.coreDataId = requestItem.record.coreDataId;
                note.deleted = false;
                activity.Notes.Add(note);
                note.modified = DateTime.Now;
                db.SaveChanges();
                return note.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateNote(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Note note = db.Notes.SingleOrDefault(i => i.id == serverId);
            if (note != null)
            {
                if (requestItem.record.recordData.ContainsKey("note"))
                {
                    string value = requestItem.record.recordData["note"];
                    note.note1 = value;
                }
                if (requestItem.record.recordData.ContainsKey("teacherEmailID"))
                {
                    string value = requestItem.record.recordData["teacherEmailID"];
                    note.teacherEmailID = value;
                }
                if (requestItem.record.recordData.ContainsKey("photo"))
                {
                    string value = requestItem.record.recordData["photo"];
                    note.photo = value;
                }
                if (requestItem.record.recordData.ContainsKey("date"))
                {
                    string value = requestItem.record.recordData["date"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        note.date = dateTime;
                    }

                }
                note.modified = DateTime.Now;
                db.SaveChanges();
                return note.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteNote(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Note note = db.Notes.SingleOrDefault(i => i.id == serverId);
            if (note != null)
            {
                note.deleted = true;
                note.modified = DateTime.Now;
                db.SaveChanges();
                return note.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region Checklist
        private string AddCheckList(SyncObjectRequestModel requestItem)
        {
            Checklist checklist = new Checklist();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == parentId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("checklistValues"))
                {
                    string value = requestItem.record.recordData["checklistValues"];
                    checklist.checklistValues = value;
                }
                if (requestItem.record.recordData.ContainsKey("checklistType"))
                {
              
                    string value = requestItem.record.recordData["checklistType"];
                    checklist.checklistType = value;



                }
                if (requestItem.record.recordData.ContainsKey("draftActivityURI"))
                {
                    string value = requestItem.record.recordData["draftActivityURI"];
                    checklist.draftActivityURI = value;
                }

                checklist.coreDataId = requestItem.record.coreDataId;
                checklist.deleted = false;
                activity.Checklists.Add(checklist);
                checklist.modified = DateTime.Now;
                db.SaveChanges();
                return checklist.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateCheckList(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Checklist checklist = db.Checklists.SingleOrDefault(i => i.id == serverId);
            if (checklist != null)
            {
                if (requestItem.record.recordData.ContainsKey("checklistValues"))
                {
                    string value = requestItem.record.recordData["checklistValues"];
                    checklist.checklistValues = value;
                }
                if (requestItem.record.recordData.ContainsKey("checklistType"))
                {
                  
                    string value = requestItem.record.recordData["checklistType"];
                    checklist.draftActivityURI = value;



                }
                if (requestItem.record.recordData.ContainsKey("draftActivityURI"))
                {
                    string value = requestItem.record.recordData["draftActivityURI"];
                    checklist.draftActivityURI = value;
                }
                checklist.modified = DateTime.Now;
                db.SaveChanges();
                return checklist.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteCheckList(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Checklist checklist = db.Checklists.SingleOrDefault(i => i.id == serverId);
            if (checklist != null)
            {
                checklist.deleted = true;
                checklist.modified = DateTime.Now;
                db.SaveChanges();
                return checklist.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region Transport
        private string AddTransport(SyncObjectRequestModel requestItem)
        {
            Transport transport = new Transport();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == parentId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("dropOffLocation"))
                {
                    string value = requestItem.record.recordData["dropOffLocation"];
                    transport.dropOffLocation = value;
                }
                if (requestItem.record.recordData.ContainsKey("pickLocation"))
                {
                    string value = requestItem.record.recordData["pickLocation"];
                    transport.pickLocation = value;
                }
                if (requestItem.record.recordData.ContainsKey("type"))
                {
                    string value = requestItem.record.recordData["type"];
                    transport.type = value;
                }
                if (requestItem.record.recordData.ContainsKey("dropOffDate"))
                {
                    string value = requestItem.record.recordData["dropOffDate"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        transport.dropOffDate = dateTime;
                    }

                }
                transport.coreDataId = requestItem.record.coreDataId;
                transport.deleted = false;
                activity.Transports.Add(transport);
                transport.modified = DateTime.Now;
                db.SaveChanges();
                return transport.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateTransport(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Transport transport = db.Transports.SingleOrDefault(i => i.id == serverId);
            if (transport != null)
            {
                if (requestItem.record.recordData.ContainsKey("dropOffLocation"))
                {
                    string value = requestItem.record.recordData["dropOffLocation"];
                    transport.dropOffLocation = value;
                }
                if (requestItem.record.recordData.ContainsKey("pickLocation"))
                {
                    string value = requestItem.record.recordData["pickLocation"];
                    transport.pickLocation = value;
                }
                if (requestItem.record.recordData.ContainsKey("type"))
                {
                    string value = requestItem.record.recordData["type"];
                    transport.type = value;
                }
                if (requestItem.record.recordData.ContainsKey("dropOffDate"))
                {
                    string value = requestItem.record.recordData["dropOffDate"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        transport.dropOffDate = dateTime;
                    }

                }
                transport.modified = DateTime.Now;
                db.SaveChanges();
                return transport.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteTransport(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Transport transport = db.Transports.SingleOrDefault(i => i.id == serverId);
            if (transport != null)
            {
                transport.deleted = true;
                transport.modified = DateTime.Now;
                db.SaveChanges();
                return transport.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region EmergencyContact
        private string AddEmergencyContact(SyncObjectRequestModel requestItem)
        {
            EmergencyContact emergencyContact = new EmergencyContact();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == parentId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    emergencyContact.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("phone"))
                {
                    string value = requestItem.record.recordData["phone"];
                    emergencyContact.phone = value;
                }
              
                emergencyContact.coreDataId = requestItem.record.coreDataId;
                emergencyContact.deleted = false;
                activity.EmergencyContacts.Add(emergencyContact);
                emergencyContact.modified = DateTime.Now;
                db.SaveChanges();
                return emergencyContact.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateEmergencyContact(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            EmergencyContact emergencyContact = db.EmergencyContacts.SingleOrDefault(i => i.id == serverId);
            if (emergencyContact != null)
            {
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    emergencyContact.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("phone"))
                {
                    string value = requestItem.record.recordData["phone"];
                    emergencyContact.phone = value;
                }

                emergencyContact.modified = DateTime.Now;
                db.SaveChanges();
                return emergencyContact.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteEmergencyContact(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            EmergencyContact emergencyContact = db.EmergencyContacts.SingleOrDefault(i => i.id == serverId);
            if (emergencyContact != null)
            {

                emergencyContact.deleted = true;
                emergencyContact.modified = DateTime.Now;
                db.SaveChanges();
                return emergencyContact.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region Photo
        private string AddPhoto(SyncObjectRequestModel requestItem)
        {
            Photo photo = new Photo();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Comment comment = db.Comments.SingleOrDefault(i => i.id == parentId);
            if (comment != null)
            {
                if (requestItem.record.recordData.ContainsKey("notes"))
                {
                    string value = requestItem.record.recordData["notes"];
                    photo.notes = value;
                }
                if (requestItem.record.recordData.ContainsKey("avatar"))
                {
                    string value = requestItem.record.recordData["avatar"];
                    photo.avatar = value;
                }

             

                photo.coreDataId = requestItem.record.coreDataId;
                photo.deleted = false;
                comment.Photos.Add(photo);
                photo.modified = DateTime.Now;
                db.SaveChanges();
                return photo.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdatePhoto(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Photo photo = db.Photos.SingleOrDefault(i => i.id == serverId);
            if (photo != null)
            {
                if (requestItem.record.recordData.ContainsKey("notes"))
                {
                    string value = requestItem.record.recordData["notes"];
                    photo.notes = value;
                }
                if (requestItem.record.recordData.ContainsKey("avatar"))
                {
                    string value = requestItem.record.recordData["avatar"];
                    photo.avatar = value;
                }
                photo.modified = DateTime.Now;
                db.SaveChanges();
                return photo.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeletePhoto(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Photo photo = db.Photos.SingleOrDefault(i => i.id == serverId);
            if (photo != null)
            {
                photo.deleted = true;
                photo.modified = DateTime.Now;
                db.SaveChanges();
                return photo.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region Location
        private string AddLocation(SyncObjectRequestModel requestItem)
        {
            Location location = new Location();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == parentId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("address"))
                {
                    string value = requestItem.record.recordData["address"];
                    location.address = value;
                }
                if (requestItem.record.recordData.ContainsKey("altitude"))
                {
                    string value = requestItem.record.recordData["altitude"];
                    location.altitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("facilities"))
                {
                    string value = requestItem.record.recordData["facilities"];
                    location.facilities = value;
                }
                if (requestItem.record.recordData.ContainsKey("latitude"))
                {
                    string value = requestItem.record.recordData["latitude"];
                    location.latitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("locationType"))
                {
                    string value = requestItem.record.recordData["locationType"];
                    location.locationType = value;
                }
                if (requestItem.record.recordData.ContainsKey("longitude"))
                {
                    string value = requestItem.record.recordData["longitude"];
                    location.longitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    location.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("notes"))
                {
                    string value = requestItem.record.recordData["notes"];
                    location.notes = value;
                }
                if (requestItem.record.recordData.ContainsKey("riskType"))
                {
                    string value = requestItem.record.recordData["riskType"];
                    location.riskType = value;
                }
                if (requestItem.record.recordData.ContainsKey("isPrimary"))
                {
                    string value = requestItem.record.recordData["isPrimary"];
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["isPrimary"], out resultint))
                    {
                        location.isPrimary = resultint;
                    }

               
                }
                  if (requestItem.record.recordData.ContainsKey("hazardRating"))
              
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["hazardRating"], out resultint))
                    {
                        location.hazardRating = resultint;
                    }



                }
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["hazardRating"], out resultint))
                    {
                        location.hazardRating = resultint;
                    }



                }
                location.coreDataId = requestItem.record.coreDataId;
                location.deleted = false;
                activity.Locations.Add(location);
                location.modified = DateTime.Now;
                db.SaveChanges();
                return location.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateLocation(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Location location = db.Locations.SingleOrDefault(i => i.id == serverId);
            if (location != null)
            {
                if (requestItem.record.recordData.ContainsKey("address"))
                {
                    string value = requestItem.record.recordData["address"];
                    location.address = value;
                }
                if (requestItem.record.recordData.ContainsKey("altitude"))
                {
                    string value = requestItem.record.recordData["altitude"];
                    location.altitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("facilities"))
                {
                    string value = requestItem.record.recordData["facilities"];
                    location.facilities = value;
                }
                if (requestItem.record.recordData.ContainsKey("latitude"))
                {
                    string value = requestItem.record.recordData["latitude"];
                    location.latitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("locationType"))
                {
                    string value = requestItem.record.recordData["locationType"];
                    location.locationType = value;
                }
                if (requestItem.record.recordData.ContainsKey("longitude"))
                {
                    string value = requestItem.record.recordData["longitude"];
                    location.longitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    location.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("notes"))
                {
                    string value = requestItem.record.recordData["notes"];
                    location.notes = value;
                }
                if (requestItem.record.recordData.ContainsKey("riskType"))
                {
                    string value = requestItem.record.recordData["riskType"];
                    location.riskType = value;
                }
                if (requestItem.record.recordData.ContainsKey("isPrimary"))
                {
                    string value = requestItem.record.recordData["isPrimary"];
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["isPrimary"], out resultint))
                    {
                        location.isPrimary = resultint;
                    }


                }
                if (requestItem.record.recordData.ContainsKey("hazardRating"))
              
                {
                    int resultint;

                    if (int.TryParse(requestItem.record.recordData["hazardRating"], out resultint))
                    {
                        location.hazardRating = resultint;
                    }



                }
                location.modified = DateTime.Now;
                db.SaveChanges();
                return location.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteLocation(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Location location = db.Locations.SingleOrDefault(i => i.id == serverId);
            if (location != null)
            {
                location.deleted = true;
                location.modified = DateTime.Now;
                db.SaveChanges();
                return location.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region Alert
        private string AddAlert(SyncObjectRequestModel requestItem)
        {
            Alert alert = new Alert();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == parentId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("email"))
                {
                    string value = requestItem.record.recordData["email"];
                    alert.email = value;
                }
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    alert.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("date"))
                {
                    string value = requestItem.record.recordData["date"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        alert.date = dateTime;
                    }

                }
                alert.coreDataId = requestItem.record.coreDataId;
                alert.deleted = false;
                activity.Alerts.Add(alert);
                alert.modified = DateTime.Now;
                db.SaveChanges();
                return alert.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateAlert(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Alert alert = db.Alerts.SingleOrDefault(i => i.id == serverId);
            if (alert != null)
            {
                if (requestItem.record.recordData.ContainsKey("email"))
                {
                    string value = requestItem.record.recordData["email"];
                    alert.email = value;
                }
                if (requestItem.record.recordData.ContainsKey("name"))
                {
                    string value = requestItem.record.recordData["name"];
                    alert.name = value;
                }
                if (requestItem.record.recordData.ContainsKey("date"))
                {
                    string value = requestItem.record.recordData["date"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        alert.date = dateTime;
                    }

                }
                alert.modified = DateTime.Now;
                db.SaveChanges();
                return alert.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteAlert(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
             Alert alert = db.Alerts.SingleOrDefault(i => i.id == serverId);
            if (alert != null)
             {
                 alert.deleted = true;
                 alert.modified = DateTime.Now;
                db.SaveChanges();
                return alert.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region GeneralWeather
        private string AddGeneralWeather(SyncObjectRequestModel requestItem)
        {
            GeneralWeather generalWeather = new GeneralWeather();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            Activity activity = db.Activities.SingleOrDefault(i => i.id == parentId);
            if (activity != null)
            {
                if (requestItem.record.recordData.ContainsKey("area"))
                {
                    string value = requestItem.record.recordData["area"];
                    generalWeather.area = value;
                }
                if (requestItem.record.recordData.ContainsKey("latitude"))
                {
                    string value = requestItem.record.recordData["latitude"];
                    generalWeather.latitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("longitude"))
                {
                    string value = requestItem.record.recordData["longitude"];
                    generalWeather.longitude = value;
                }

                if (requestItem.record.recordData.ContainsKey("dateRetrieved"))
                {
                    string value = requestItem.record.recordData["dateRetrieved"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        generalWeather.dateRetrieved = dateTime;
                    }

                }

                generalWeather.coreDataId = requestItem.record.coreDataId;
                generalWeather.deleted = false;
                activity.GeneralWeathers.Add(generalWeather);
                generalWeather.modified = DateTime.Now;
                db.SaveChanges();
                return generalWeather.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateGeneralWeather(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            GeneralWeather generalWeather = db.GeneralWeathers.SingleOrDefault(i => i.id == serverId);
            if (generalWeather != null)
            {
                if (requestItem.record.recordData.ContainsKey("area"))
                {
                    string value = requestItem.record.recordData["area"];
                    generalWeather.area = value;
                }
                if (requestItem.record.recordData.ContainsKey("latitude"))
                {
                    string value = requestItem.record.recordData["latitude"];
                    generalWeather.latitude = value;
                }
                if (requestItem.record.recordData.ContainsKey("longitude"))
                {
                    string value = requestItem.record.recordData["longitude"];
                    generalWeather.longitude = value;
                }

                if (requestItem.record.recordData.ContainsKey("dateRetrieved"))
                {
                    string value = requestItem.record.recordData["dateRetrieved"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        generalWeather.dateRetrieved = dateTime;
                    }

                }

                generalWeather.modified = DateTime.Now;
                db.SaveChanges();
                return generalWeather.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteGeneralWeather(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            GeneralWeather generalWeather = db.GeneralWeathers.SingleOrDefault(i => i.id == serverId);
            if (generalWeather != null)
            {
                generalWeather.deleted = true;
                generalWeather.modified = DateTime.Now;
                db.SaveChanges();
                return generalWeather.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
        #region Forecast
        private string AddForecast(SyncObjectRequestModel requestItem)
        {
            Forecast forecast = new Forecast();
            int parentId;
            bool result = Int32.TryParse(requestItem.parent.serverId, out parentId);
            if (!result)
            {
                return "-1";

            }
            GeneralWeather generalWeather = db.GeneralWeathers.SingleOrDefault(i => i.id == parentId);
            if (generalWeather != null)
            {
                if (requestItem.record.recordData.ContainsKey("grassfire"))
                {
                    string value = requestItem.record.recordData["grassfire"];
                    forecast.grassfire = value;
                }
                if (requestItem.record.recordData.ContainsKey("forestfire"))
                {
                    string value = requestItem.record.recordData["forestfire"];
                    forecast.forestfire = value;
                }
                if (requestItem.record.recordData.ContainsKey("heading"))
                {
                    string value = requestItem.record.recordData["heading"];
                    forecast.heading = value;
                }
                if (requestItem.record.recordData.ContainsKey("maxTemp"))
                {
                    string value = requestItem.record.recordData["maxTemp"];
                    forecast.maxTemp = value;
                }
                if (requestItem.record.recordData.ContainsKey("minTemp"))
                {
                    string value = requestItem.record.recordData["minTemp"];
                    forecast.minTemp = value;
                }
                if (requestItem.record.recordData.ContainsKey("notes"))
                {
                    string value = requestItem.record.recordData["notes"];
                    forecast.notes = value;
                }
                if (requestItem.record.recordData.ContainsKey("windDirection"))
                {
                    string value = requestItem.record.recordData["windDirection"];
                    forecast.notes = value;
                }
                if (requestItem.record.recordData.ContainsKey("windSpeed"))
                {
                    string value = requestItem.record.recordData["windSpeed"];
                    forecast.windSpeed = value;
                }
                if (requestItem.record.recordData.ContainsKey("date"))
                {
                    string value = requestItem.record.recordData["date"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        forecast.date = dateTime;
                    }

                }

                forecast.coreDataId = requestItem.record.coreDataId;
                forecast.deleted = false;
                generalWeather.Forecasts.Add(forecast);
                forecast.modified = DateTime.Now;
                db.SaveChanges();
                return forecast.id.ToString(); ;
            }
            return "-1";
        }
        private string UpdateForecast(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
            Forecast forecast = db.Forecasts.SingleOrDefault(i => i.id == serverId);
            if (forecast != null)
            {
                if (requestItem.record.recordData.ContainsKey("grassfire"))
                {
                    string value = requestItem.record.recordData["grassfire"];
                    forecast.grassfire = value;
                }
                if (requestItem.record.recordData.ContainsKey("forestfire"))
                {
                    string value = requestItem.record.recordData["forestfire"];
                    forecast.forestfire = value;
                }
                if (requestItem.record.recordData.ContainsKey("heading"))
                {
                    string value = requestItem.record.recordData["heading"];
                    forecast.heading = value;
                }
                if (requestItem.record.recordData.ContainsKey("maxTemp"))
                {
                    string value = requestItem.record.recordData["maxTemp"];
                    forecast.maxTemp = value;
                }
                if (requestItem.record.recordData.ContainsKey("minTemp"))
                {
                    string value = requestItem.record.recordData["minTemp"];
                    forecast.minTemp = value;
                }
                if (requestItem.record.recordData.ContainsKey("notes"))
                {
                    string value = requestItem.record.recordData["notes"];
                    forecast.notes = value;
                }
                if (requestItem.record.recordData.ContainsKey("windDirection"))
                {
                    string value = requestItem.record.recordData["windDirection"];
                    forecast.notes = value;
                }
                if (requestItem.record.recordData.ContainsKey("windSpeed"))
                {
                    string value = requestItem.record.recordData["windSpeed"];
                    forecast.windSpeed = value;
                }
                if (requestItem.record.recordData.ContainsKey("date"))
                {
                    string value = requestItem.record.recordData["date"];
                    DateTime dateTime;
                    if (DateTime.TryParse(value, out dateTime))
                    {
                        forecast.date = dateTime;
                    }

                }

                forecast.modified = DateTime.Now;
                db.SaveChanges();
                return forecast.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        private string DeleteForecast(SyncObjectRequestModel requestItem)
        {
            int serverId;
            bool result = Int32.TryParse(requestItem.record.serverId, out serverId);
            if (!result)
            {
                return "-1";

            }
           Forecast forecast = db.Forecasts.SingleOrDefault(i => i.id == serverId);
            if (forecast != null)
            {
                forecast.deleted = true;
                forecast.modified = DateTime.Now;
                db.SaveChanges();
                return forecast.id.ToString(); ;
            }
            else
            {
                return "-1";
            }

        }
        #endregion
    }

}
