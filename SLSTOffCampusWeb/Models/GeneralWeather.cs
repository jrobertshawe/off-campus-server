//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SLSTOffCampusWeb.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GeneralWeather
    {
        public GeneralWeather()
        {
            this.Forecasts = new HashSet<Forecast>();
        }
    
        public int id { get; set; }
        public Nullable<int> activityId { get; set; }
        public string area { get; set; }
        public Nullable<System.DateTime> dateRetrieved { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public Nullable<bool> deleted { get; set; }
        public Nullable<System.DateTime> modified { get; set; }
        public string coreDataId { get; set; }
    
        public virtual ICollection<Forecast> Forecasts { get; set; }
        public virtual Activity Activity { get; set; }
    }
}
