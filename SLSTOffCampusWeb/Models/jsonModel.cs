﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SLSTOffCampusWeb.Models
{
    public class registerUser
    {
        public string email;
        public string password;
        public string firstname;
        public string lastname;
        public string cellPhone;
        public string homePhone;

    }
    public class registerUserResponseModel
    {

        public string token { get; set; }
        public string errorMsg { get; set; }
        public int error { get; set; }
    }
    public class SyncObjectRequestModelParent
    {
        public string serverId { get; set; }
        public string tableName { get; set; }

    }
    public class SyncChangesRequestModel
    {
        public string appName { get; set; }
        public string updateTime { get; set; }
        public Dictionary<string, string> accessCriteria { get; set; }

    }
    public class SyncObjectRequestModelObject
    {
        public string serverId { get; set; }
        public string tableName { get; set; }
        public string coreDataId { get; set; }
        public Dictionary<string, string> recordData { get; set; }

    }
    public class SyncObjectRequestModelAccess
    {
        public Dictionary<string, string> objectData { get; set; }

    }
    public class SyncObjectRequestModel
    {
        public string action { get; set; }
        public string appname { get; set; }
        public SyncObjectRequestModelParent parent { get; set; }
        public SyncObjectRequestModelObject record { get; set; }
        public Dictionary<string, string> accessCriteria { get; set; }

    }
    public class SyncObjectChangeModel
    {
        public string updateTime { get; set; }
        public List<ResponseRecordModel> record { get; set; }
        public SyncObjectChangeModel()
        {

            //    response = new responseModel();
            record = new List<ResponseRecordModel>();


        }

    }
    public class StatusResponseModel
    {

        public string errorMsg { get; set; }
        public int error { get; set; }
    }
    public class RecordModel
    {

        public string serverId { get; set; }
        public string tableName { get; set; }
        public string coreDataId { get; set; }
    }
    public class ResponseRecordModel
    {
        public SyncObjectRequestModelParent parent { get; set; }
        public SyncObjectRequestModelObject record { get; set; }
        public ResponseRecordModel()
        {

            //    response = new responseModel();
            record = new SyncObjectRequestModelObject();
            record.recordData = new Dictionary<string, string>();
            parent = new SyncObjectRequestModelParent();

        }
    }
    public class ResponseModel
    {

        public RecordModel record { get; set; }
        public ResponseModel()
        {

            //    response = new responseModel();
            record = new RecordModel();


        }
    }
    public class SyncRecordResponseModel
    {
        public StatusResponseModel status { get; set; }
        public ResponseModel response { get; set; }
        public SyncRecordResponseModel()
        {

            //    response = new responseModel();
            response = new ResponseModel();
            status = new StatusResponseModel();


        }
    }
    public class SyncChangesResponse
    {
        public StatusResponseModel status { get; set; }
        public SyncObjectChangeModel response { get; set; }
        public SyncChangesResponse()
        {

            //    response = new responseModel();
            response = new SyncObjectChangeModel();
            status = new StatusResponseModel();


        }
    }
     public class FeedBackRequest
     {
         public string name;
         public string title;
         public string email;
         public string description;

     }
     
}